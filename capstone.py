student_list = ['belle', 'ariel', 'aurora', 'anna', 'tiana', 'merida']
student_grade = [60, 80, 95, 85, 89, 97]

def continue_to_menu():
    print(input('\nPress ENTER to continue'))
    main_menu()

def main_menu():
    menu = """
    [program begins]
    Menu:
    1. Add a new student & grade
    2. Print all students
    3. Print all grades
    4. Lookup a student's grade
    5. Find student with highest grade
    6. Find student with lowest grade
    7. Generate graph
    8. Quit
    """
    print(menu)
    option = int(input("Choose an option: "))
    if option == 1:
        name = input("New student name: ")
        student_list.append(name)
        grade = int(input("New student grades: "))
        student_grade.append(grade)
        continue_to_menu()

    elif option == 2:
        print("All students: "+str(student_list))
        continue_to_menu()

    elif option == 3:
        print("All grades: "+str(student_grade))
        continue_to_menu()

    elif option == 4:
        name = input("Look up which student? ")
        if name in student_list:
            index = student_list.index(name)
            print(f"Student {name} has grade {student_grade[index]}")
        else:
            print("Student does not exist")
        continue_to_menu()

    elif option == 5:
        max_grade = max(student_grade)
        index = student_grade.index(max_grade)
        print(f"The student with highest grade is {student_list[index]}")
        print(f"His/Her grade is {student_grade[index]}")
        continue_to_menu()

    elif option ==6:
        min_grade = min(student_grade)
        index = student_grade.index(min_grade)
        print(f"The student with lowest grade is {student_list[index]}")
        print(f"His/Her grade is {student_grade[index]}")
        continue_to_menu()

    elif option == 7:
        import matplotlib.pyplot as plt
        import numpy as np
        graph = [0,0,0,0,0]
        for i in range(len(student_grade)):
            if student_grade[i]>=60 and student_grade[i]<=70:
                graph[0] += 1
            elif student_grade[i]>=71 and student_grade[i]<=80:
                graph[1] += 1
            elif student_grade[i]>=81 and student_grade[i]<=90:
                graph[2] += 1
            elif student_grade[i]>=91 and student_grade[i]<=95:
                graph[3] += 1
            elif student_grade[i]>=96 and student_grade[i]<=100:
                graph[4] += 1
            else:
                print("less than 60")
              
        y = np.array(graph)
        mylabels = ["60-70 (lowest)", "71-80", "81-90", "91-95","96-100 (highest)"]
        plt.pie(y,labels = mylabels)
        plt.legend()
        plt.show() 
        continue_to_menu()

    elif option == 8:
        print("Exiting the program")

    else:
        print("Enter correct option")

main_menu()

